#include <iostream>
#include <cmath>
using namespace std;

int main() {
    
    cout << "What operation?: " << endl;
    cout << "1) Addition  2)Subtraction 3) Multiplication 4) Division 5) Exponent 6)Log" << endl;

    int choice = 0;
    
    cout << "Choice: " << endl;
    cin >> choice;
    
//Addition block
    if (choice == 1) {
    int addOne;
    int addTwo;
    cout << "Number one: " << endl;
    cin >> addOne;
    cout << "Number two: " << endl;
    cin >> addTwo;
    cout << "Sum: " << addOne + addTwo << endl;
    }
//Subtraction block
    if (choice == 2) {
    int subOne;
    int subTwo;
    cout << "Number one: " << endl;
    cin >> subOne;
    cout << "Number two: " << endl;
    cin >> subTwo;
    cout << "Difference: " << subOne - subTwo << endl;
    }
//Multiplication block
    if (choice == 3) {
    int multOne;
    int multTwo;
    cout << "Number one: " << endl;
    cin >> multOne;
    cout << "Number two: " << endl;
    cin >> multTwo;
    cout << "Product: " << multOne * multTwo << endl;
    }
//Division block
    if (choice == 4) {
    int divOne;
    int divTwo;
    cout << "Number one: " << endl;
    cin >> divOne;
    cout << "Number two: " << endl;
    cin >> divTwo;
    cout << "Quotient: " << divOne / divTwo << endl;
    }

//Exponent block
    if (choice == 5) {
    int expOne;
    int expTwo;
    cout << "Base: " << endl;
    cin >> expOne;
    cout << "Exponent: " << endl;
    cin >> expTwo;
    cout << "Answer: " << pow(expOne, expTwo) << endl;
    }

//Log block
    if (choice == 6) {
    int logOne;
    int logTwo;
    cout << "Base: " << endl;
    cin >> logOne;
    cout << "Constant: " << endl;
    cin >> logTwo;
    cout << "Answer: " << log(logTwo)/log(logOne) << endl;
    }

//Catch-All
    if ( (choice != 1)&&(choice !=2)&&(choice !=3)&&(choice !=4)&&(choice !=5)&&(choice !=6) ){
      cout << "No" << endl;  
    }
}
